package com.tweakdeveloper.android.SmileTime;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;

public class WidgetProvider extends AppWidgetProvider {

  @Override
  public void onUpdate(Context context, AppWidgetManager mgr, int myWidgetIds[]) {
    ComponentName widget = new ComponentName(context, WidgetProvider.class);
    int[] widgetIds = mgr.getAppWidgetIds(widget);
    Intent intent = new Intent(context, WidgetService.class);
    intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIds);
    context.startService(intent);
  }

}
