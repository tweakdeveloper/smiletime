package com.tweakdeveloper.android.SmileTime;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import com.tweakdeveloper.android.SmileTime.FRAGMENTS.ClicksFragment;

public class MainActivity extends ActionBarActivity {
  
  boolean mIsDualPane;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    View clicksFragment = findViewById(R.id.clicksFragment);
    mIsDualPane = clicksFragment != null &&
      clicksFragment.getVisibility() == View.VISIBLE;
  }
  
  public void handleSmileyTap() {
    if(mIsDualPane) {
      FragmentManager mgr = getSupportFragmentManager();
      ClicksFragment clicksFragment = (ClicksFragment)mgr
        .findFragmentById(R.id.clicksFragment);
      clicksFragment.updateText();
    } else {
      startActivity(new Intent(this, ClicksActivity.class));
    }
  }

}
