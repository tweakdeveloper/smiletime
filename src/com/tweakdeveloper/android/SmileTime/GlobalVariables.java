package com.tweakdeveloper.android.SmileTime;

public class GlobalVariables {

  public static int i = 0;
  public static boolean hatToggle = true;
  public static int smileys[] = {
      R.drawable.smileynew, R.drawable.smileynewangry, R.drawable.smileynewsad,
      R.drawable.smileynewscared, R.drawable.smileyjack,
      R.drawable.smileynolan, R.drawable.smileykira, R.drawable.smileyheart,
      R.drawable.smileynick, R.drawable.smileyhardcore, R.drawable.smileyodoto
  };
  public static int hats[] = {
      R.drawable.hatnew, R.drawable.hatred, R.drawable.hatblu,
      R.drawable.hatyellow, R.drawable.hatgreen, R.drawable.hathelmet,
      R.drawable.hathalo, R.drawable.hathelmetblack, R.drawable.hatorange
  };
  public static int messages[] = {
      R.string.nice_day, R.string.do_it, R.string.gives_lemons,
      R.string.flex_muscles, R.string.if_u_believe, R.string.lot_worse,
      R.string.wish_is_command, R.string.they_can, R.string.go_for_it,
      R.string.orange_juice, R.string.the_cake, R.string.life, R.string.space,
      R.string.pointless, R.string.evil_genius
  };
}
