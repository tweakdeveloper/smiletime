package com.tweakdeveloper.android.SmileTime;

import android.app.IntentService;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;

public class WidgetService extends IntentService {

  public WidgetService() {
    super("WidgetService");
  }

  private SharedPreferences prefs;
  private int widgetIDs[];
  
  @Override
  public void onStart(Intent intent, int startId) {
    widgetIDs = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);
    super.onStart(intent, startId);
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    AppWidgetManager mgr = AppWidgetManager.getInstance(this);
    prefs = PreferenceManager.getDefaultSharedPreferences(this);
    int message = prefs.getInt(getString(R.string.key_current_message), 0) + 1;
    if(GlobalVariables.messages.length <= message) {
      message = 0;
    }
    SharedPreferences.Editor edit = prefs.edit();
    edit.putInt(getString(R.string.key_current_message), message);
    edit.commit();
    String msg = getString(GlobalVariables.messages[message]);
    for(int widgetID : widgetIDs) {
      RemoteViews remoteViews = new RemoteViews(getPackageName(),
        R.layout.widget_layout);
      remoteViews.setTextViewText(R.id.message_view, msg);
      Intent i = new Intent(this, WidgetProvider.class);
      i.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
      i.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, widgetIDs);
      PendingIntent pIntent = PendingIntent.getBroadcast(this, 0, i,
        PendingIntent.FLAG_UPDATE_CURRENT);
      remoteViews.setOnClickPendingIntent(R.id.icon_view, pIntent);
      mgr.updateAppWidget(widgetID, remoteViews);
    }
  }

}
