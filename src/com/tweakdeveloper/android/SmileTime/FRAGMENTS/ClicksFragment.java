package com.tweakdeveloper.android.SmileTime.FRAGMENTS;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.ShareActionProvider;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.tweakdeveloper.android.SmileTime.GlobalVariables;
import com.tweakdeveloper.android.SmileTime.R;

public class ClicksFragment extends Fragment {
  
  private ShareActionProvider mProvider;
  private SharedPreferences prefs;
  private TextView tv;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
    GlobalVariables.i = prefs.getInt(getString(R.string.key_current_message), 0);
    setHasOptionsMenu(true);
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_clicks, container);
    tv = (TextView) v.findViewById(R.id.textView);
    updateText();
    return v;
  }

  public void updateText() {
    tv.setText(getString(GlobalVariables.messages[GlobalVariables.i++]));
    if (GlobalVariables.i >= GlobalVariables.messages.length)
      GlobalVariables.i = 0;
  }

  @Override
  public void onStop() {
    super.onStop();
    Editor editor = prefs.edit();
    editor.putInt(getString(R.string.key_current_message), GlobalVariables.i);
    editor.commit();
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.activity_clicks, menu);
    MenuItem shareItem = menu.findItem(R.id.sharesend);
    mProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(shareItem);
    Intent tmp = new Intent(Intent.ACTION_SEND);
    tmp.setType("text/plain");
    tmp.putExtra(Intent.EXTRA_TEXT, getString(GlobalVariables
      .messages[GlobalVariables.i]));
    mProvider.setShareIntent(tmp);
  }

  public void textViewOnClick(View v) {
    getActivity().finish();
  }

}
