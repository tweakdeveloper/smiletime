package com.tweakdeveloper.android.SmileTime.FRAGMENTS;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.tweakdeveloper.android.SmileTime.AboutActivity;
import com.tweakdeveloper.android.SmileTime.GlobalVariables;
import com.tweakdeveloper.android.SmileTime.MainActivity;
import com.tweakdeveloper.android.SmileTime.PrefsActivity;
import com.tweakdeveloper.android.SmileTime.R;

public class MainFragment extends Fragment {

  SharedPreferences prefs;
  ImageView smileyView;
  ImageView hatView;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setHasOptionsMenu(true);
    prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
      Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.fragment_main, container);
    smileyView = (ImageView) v.findViewById(R.id.smileyView);
    hatView = (ImageView) v.findViewById(R.id.hatView);
    smileyView.setOnClickListener(new OnClickListener() {

      @Override
      public void onClick(View view) {
        ((MainActivity)getActivity()).handleSmileyTap();
      }
    });
    return v;
  }

  @Override
  public void onResume() {
    super.onResume();
    int smiley, hat;
    Boolean hat_visible;
    String smiley_pref, hat_pref;
    smiley_pref = prefs.getString(getString(R.string.preference_smiley), "0");
    hat_pref = prefs.getString(getString(R.string.preference_hat), "0");
    hat_visible = prefs
        .getBoolean(getString(R.string.preference_visible), true);
    smiley = Integer.parseInt(smiley_pref);
    hat = Integer.parseInt(hat_pref);
    toggleHat(true, hat_visible);
    smileyView.setImageResource(GlobalVariables.smileys[smiley]);
    hatView.setImageResource(GlobalVariables.hats[hat]);
  }

  @Override
  public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    super.onCreateOptionsMenu(menu, inflater);
    inflater.inflate(R.menu.activity_main, menu);
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
      case R.id.settings:
        Intent settingsIntent = new Intent(getActivity(), PrefsActivity.class);
        startActivity(settingsIntent);
        return true;
      case R.id.aboutApp:
        Intent aboutIntent = new Intent(getActivity(), AboutActivity.class);
        startActivity(aboutIntent);
        return true;
      default:
        return super.onOptionsItemSelected(item);
    }
  }

  public void toggleHat(Boolean check, Boolean checkVal) {
    if (check) {
      if (checkVal) {
        GlobalVariables.hatToggle = true;
        hatView.setVisibility(View.VISIBLE);
      } else {
        GlobalVariables.hatToggle = false;
        hatView.setVisibility(View.INVISIBLE);
      }
    } else {
      if (GlobalVariables.hatToggle) {
        GlobalVariables.hatToggle = false;
        hatView.setVisibility(View.INVISIBLE);
      } else {
        GlobalVariables.hatToggle = true;
        hatView.setVisibility(View.VISIBLE);
      }
    }
  }

}
