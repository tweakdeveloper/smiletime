package com.tweakdeveloper.android.SmileTime.FRAGMENTS;

import android.os.Bundle;
import android.support.v4.preference.PreferenceFragment;
import com.tweakdeveloper.android.SmileTime.R;


public class SettingsFragment extends PreferenceFragment {
  
  @Override
  public void onCreate(Bundle paramBundle) {
    super.onCreate(paramBundle);
    addPreferencesFromResource(R.xml.preference_smiletime);
  }

}
